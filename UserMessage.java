package ChatSever ; 
import java.util.Vector;


public class UserMessage extends ChatMessage {

	public Vector<AbstractUser> recipients = new Vector<AbstractUser>();
	public AbstractUser  recipientUser  ;

	public void UserMessage(AbstractUser sender, AbstractUser recipient, String message) {

		setMessage(message);
		setSender(sender);
		recipientUser = recipient  ;
	}

	public void UserMessage(AbstractUser sender, Vector<AbstractUser> recipientsList, String message) {

		setMessage(message);
		setSender(sender);

		for ( int i = 0 ; i  < recipientsList.size() ; i ++)
			recipients.add(recipientsList.get(i)) ;


	}

}