package  ChatSever;

import java.util.Vector;

public interface IChatServer {

	
	
  public void addUser(GeneralUser user);

  public void addRoomAdmin(AdminUser admin, Vector<String> permissions  , int UserID);

  public void removeUser(int userID);

  public void removeAllUser(int roomID);

  public void removeRoom(int roomID);

  public Vector<AbstractRoom> getRooms();

  public void joinRoom(int roomID, AbstractUser  newUser);

  public void removeAllRooms(Vector<AbstractRoom> Rooms);

  public void creatRestirctedRoom(String title, String desc, Vector<AbstractUser> allowdUsers);

  public void createGenralRoom();

  public void sendMessage(ChatMessage message);

  public void leaveRoom(int roomID, int userID);

  public ChatServer getInstance();

  public void loadPersistanceConfigurations();

  public void blockUser(int userID, int blockedUserID);



 }