package ChatSever;

import java.util.Vector;

//import String;

public class AbstractRoom {

  public String title;
  public String desciption;
  public Vector<AbstractUser> users;
  public int id;
  AdminUser  admin = new AdminUser() ;
  
  public void setTitle(String Title) {
 
   title = Title   ; 
  }

  public String getTitle() {
  return title;
  }

  public void setDecription(String desc) {
  desciption  = desc   ;
  }

  public String getDesciption() {
  return  desciption ;
  }

  public Vector<AbstractUser> getUsers() {
  return users  ;
  }

  public void setID(int roomId) {
   id = roomId ;
  }

  public int getID() {
  return id;
  }

  public void setAdmin   (AdminUser  newAdmin){
	  admin = newAdmin ; 
  }
  
  public AdminUser getAdmin (){
	  return  admin ;
  }
  
}