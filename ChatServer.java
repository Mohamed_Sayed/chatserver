package ChatSever;

import java.util.Vector;

import org.omg.PortableInterceptor.USER_EXCEPTION;

//import GeneralRoom;
//import RestrictedRoom;



//import ChatServer.AbstractRoom;
//import ChatServer.AbstractUser;



public class ChatServer implements IChatServer {

	public Vector<AbstractRoom> rooms = new Vector<AbstractRoom>();
	public Vector<GeneralUser> User = new Vector<GeneralUser> () ;
	private static ChatServer chatServer;

	//  public IPersistanceMechanism persistanceMechanism;



	public void addUser(GeneralUser user) {
		User.add (user) ;   
	}

	public void addRoomAdmin(AdminUser admin,  Vector<String> permissions  , int RoomID) {

		admin.addPermission(permissions);

		for ( int i = 0 ; i < rooms.size() ; i ++)
			if (rooms.get(i).id == RoomID){
				rooms.get(i).setAdmin(admin);
				break ;
			}		 
	}


	public void removeUser(int userID) {
		for (  int i = 0   ; i  < User.size()  ;  i ++){
			if (User.get(i).id == userID){
				User.remove(i) ; 
				break ;
			}
		}
	}


	public void removeAllUser(int roomID) {

		for (int i = 0; i < rooms.size()  ; i ++){ 
			if (rooms.get(i).id == roomID){
				rooms.get(i).users.removeAllElements();
				break ;
			}

		}
	}


	public void removeRoom(int roomID) {
		for ( int i = 0 ; i < rooms.size() ; i ++){
			if (rooms.get(i).id == roomID){

				rooms.remove(i) ; 
				break ; 
			}

		}

	}



	public Vector<AbstractRoom> getRooms() {  

		return rooms;
	}


	public void joinRoom(int roomID, AbstractUser newUser) {

		for ( int i = 0 ; i < rooms.size() ; i ++){

			if (rooms.get(i).id == roomID ){
				rooms.get(i).users.add(newUser) ; 
				break ;
			}
		}

	}


	public void removeAllRooms(Vector<AbstractRoom> Rooms ) {

		Rooms.removeAllElements();
	}


	public void creatRestirctedRoom(String title, String desc, Vector<AbstractUser> allowdUsers) {
		RestrictedRoom  newRoom = new RestrictedRoom() ; 
		newRoom.setTitle(title);
		newRoom.setDecription(desc);
		
		for ( int i = 0 ; i < allowdUsers.size() ; i ++)
		newRoom.allowedUsers.add(allowdUsers.get(i) ) ;
		
		rooms.add(newRoom) ; 

	}

	public void createGenralRoom() {
		GeneralRoom  newRoom = new GeneralRoom() ; 
		rooms.add(newRoom) ; 
	}


	public void sendMessage(ChatMessage message) {

	}

	public void leaveRoom(int roomID, int userID) {

		for ( int i = 0 ; i < rooms.size() ;  i ++){
			if (rooms.get(i).id == roomID){
				for (int j = 0 ; j < rooms.get(i).users.size() ; j ++){
					if (rooms.get(i).users.get(j).id == userID){
						rooms.get(i).users.remove(j) ; 
						break ;
					}

				}
				break; 
			}
		}

	}


	public ChatServer getInstance() {
		return chatServer;
	}

	public void loadPersistanceConfigurations() {

	}


	public void blockUser(int userID, int blockedUserID) {

		for ( int i = 0 ; i < User.size() ; i ++){
			if (userID == User.get(i).id){

				for (int j = 0 ; j < User.size()  ; j ++){
					if (User.get(j).id == blockedUserID){

						User.get(i).addBlockedUser(User.get(j));
						break ;
					}
				}
				break ;
			}
		}
	}




}